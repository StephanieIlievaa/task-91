# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣
# JavaScript Task

## Objective
* Checkout the dev branch
* Create a **".message"** for each lyric every time the beat kicks
* When implemented merge the dev branch into master.
## Requirements
* The project starts with **npm run start**.
* There must be a **"_beat"** instance in the Application class.
* The Beat class must emit the Beat.events.BIT on every interval.
* The Application class must have **"_create"** method.
* A **.message** should be created for each lyric every time the event is emitted.
* There must be an event listener in the Application class for  Beat.events.BIT 
* there must be an import statement for  eventemitter3 in the Beat
* The Beat class must extend EventEmitter

